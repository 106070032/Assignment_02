var loseState = {
    create: function() {
    // Add a background image
    game.add.image(0, 0, 'backgroundmenu');
    // Display the name of the game
    var nameLabel = game.add.text(game.width/2, 80, 'You Lose!!',{ font: '50px Arial', fill: '#ffffff' });
    nameLabel.anchor.setTo(0.5, 0.5);
    // Show the score at the center of the screen
    var score =Number(localStorage.getItem('recent_score'));
    game.totalCoin += score;
    var scoreLabel = game.add.text(game.width/2, game.height/2+50,'score: ' + score, { font: '25px Arial', fill: 'black' });
    scoreLabel.anchor.setTo(0.5, 0.5);
    // Explain how to start the game
    var startLabel = game.add.text(game.width/2, game.height-60,
        'press Enter to menu', { font: '25px Arial', fill: '#ffffff' });
    startLabel.anchor.setTo(0.5, 0.5);
    // Create a new Phaser keyboard variable: the up arrow key
    // When pressed, call the 'start'
    var upKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
    upKey.onDown.add(this.start, this);
    },
    start: function() {
    // Start the actual game
    game.bgmSound.pause();
    game.state.start('menu');
    },
}; 