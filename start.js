var startState = {
    create: function() {
    // Add a background image
    game.add.image(0, 0, 'background');
    ///this.bgmSound = game.add.audio('start_bgm');
    ///this.bgmSound.play();
    ///this.bgmSound.loop = true; 
    game.totalCoin = 0;
    game.damageIDX = 1;
    game.heartIDX = 1;
    game.shootSpeedIDX = 1;
    game.damage = 1;
    game.heart = 10;
    game.shootSpeed = 500;


    game.add.button(game.world.centerX - 95, 400, 'startbutton', this.start, this, 2, 1, 0);
    var player = game.add.sprite(0, 300, 'playerInMenu');
    game.add.tween(player).to({y: 295}, 500).to({y:
        305}, 1000).to({y: 300}, 500).loop().start();

    var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
    upKey.onDown.add(this.start, this);
    },

    start: function() {
    // Start the actual game
    localStorage.setItem('maxLevel', 1);
    localStorage.setItem('recent_score', 0);
    
    game.time.events.add(
        500, // Start callback after 1000ms.
        function() {
            //this.bgmSound.pause();
            game.state.start('intro');
        }, // Callback
        this);
    
    },

}; 