var menuState = {
    create: function() {
    // Add a background image
    game.add.image(0, 0, 'backgroundmenu');
    this.bgmSound = game.add.audio('menu_bgm');
    this.bgmSound.play();
    this.bgmSound.loop = true; 
    // Display the name of the game
    var btn1 = game.add.button(game.world.centerX - 200, 200, 'levelbutton1', this.start, this, 2, 1, 0);
    var btn2 = game.add.button(game.world.centerX - 100, 200, 'levelbutton2', this.start2, this, 2, 1, 0);
    var btn3 = game.add.button(game.world.centerX - 0, 200, 'levelbutton3', this.start3, this, 2, 1, 0);
    var btn4 = game.add.button(game.world.centerX + 100, 200, 'levelbutton4', this.start4, this, 2, 1, 0);
    var nameLabel = game.add.text(game.width/2, 80, 'Choose Level',
    { font: '50px Arial', fill: 'black' });

    nameLabel.anchor.setTo(0.5, 0.5);

    var MaxLevel = localStorage.getItem('maxLevel');
    if(MaxLevel ==1){
        btn3.alpha = 0.5;
        btn2.alpha = 0.5;
        btn4.alpha = 0.5;
        btn2.inputEnabled = false;
        btn3.inputEnabled = false;
        btn4.inputEnabled = false;
    }
    else if(MaxLevel==2){
        btn3.alpha = 0.5;
        btn2.alpha = 1;
        btn4.alpha = 0.5;
        btn2.inputEnabled = true;
        btn3.inputEnabled = false;
        btn4.inputEnabled = false;
    }
    else if(MaxLevel ==3){
        btn3.alpha = 1;
        btn2.alpha = 1;
        btn4.alpha = 0.5;
        btn2.inputEnabled = true;
        btn3.inputEnabled = true;
        btn4.inputEnabled = false;

    }
    else if(MaxLevel ==4){
        btn3.alpha = 1;
        btn2.alpha = 1;
        btn4.alpha = 1;
        btn2.inputEnabled = true;
        btn3.inputEnabled = true;
        btn4.inputEnabled = true;
    }


    // Show the score at the center of the screen
    
    var score = localStorage.getItem('recent_score');
    var table = game.add.text(game.width/2-250, game.height-80,'Each costs 15 coins' , { font: '20px Arial', fill: '#ffffff' });
    table.anchor.setTo(0.5,0.5)
    this.scoreLabel = game.add.text(game.width/2, game.height-80,'Coin: ' + game.totalCoin, { font: '25px Arial', fill: '#ffffff' });
    this.scoreLabel.anchor.setTo(0.5, 0.5);
    // Explain how to start the game
    this.damageLabel = game.add.text(game.width/2-150, game.height-60,': '+game.damageIDX, { font: '25px Arial', fill: '#ffffff' });
    this.shootSpeedLabel = game.add.text(game.width/2+50, game.height-60,': '+game.shootSpeedIDX, { font: '25px Arial', fill: '#ffffff' });
    this.heartLabel = game.add.text(game.width/2+250, game.height-60,': '+game.heartIDX, { font: '25px Arial', fill: '#ffffff' });
    var addDamage = game.add.button(game.width/2-200, game.height-60, 'damage', this.add_damage, this, 2, 1, 0);
    var addShootingSpeed = game.add.button(game.width/2, game.height-63, 'shooting_speed', this.add_shootSpeed, this, 2, 1, 0);
    var addHeart = game.add.button(game.width/2+200, game.height-60, 'heart', this.add_heart, this, 2, 1, 0);

    //his.startLabel.anchor.setTo(0.5, 0.5);
    game.add.tween(nameLabel).to({angle: -2}, 500).to({angle:
        2}, 1000).to({angle: 0}, 500).loop().start();
    // Create a new Phaser keyboard variable: the up arrow key
    // When pressed, call the 'start'
    var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
    upKey.onDown.add(this.start, this);
    },
    start: function() {
    // Start the actual game
    this.bgmSound.pause();
    game.state.start('play');
    },
    start2: function() {
        // Start the actual game
        this.bgmSound.pause();
        game.state.start('play2');
    },
    start3:function(){
        this.bgmSound.pause();
        game.state.start('play3');
    },
    start4:function(){
        this.bgmSound.pause();
        game.state.start('play4');
    },
    add_damage:function(){
        if(game.totalCoin>=15 && game.damageIDX<5){
            game.totalCoin -=15;
            this.scoreLabel.text =  'Coin: ' + game.totalCoin;
            game.damageIDX+=1;
            game.damage+=1;
            this.damageLabel.text = ': '+game.damageIDX;


        }
    },
    add_shootSpeed:function(){
        if(game.totalCoin>=15 && game.shootSpeedIDX<5){
            game.totalCoin -=15;
            this.scoreLabel.text =  'Coin: ' + game.totalCoin;
            game.shootSpeedIDX+=1;
            game.shootSpeed-=70;
            this.shootSpeedLabel.text = ': '+game.shootSpeedIDX;


        }
    },
    add_heart:function(){
        if(game.totalCoin>=15 && game.heartIDX<5){
            game.totalCoin -=15;
            this.scoreLabel.text =  'Coin: ' + game.totalCoin;
            game.heartIDX+=1;
            game.heart-=1;
            this.heartLabel.text = ': '+game.heartIDX;


        }
    },

}; 