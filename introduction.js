var introState = {
    create: function() {
    // Add a background image
    game.add.image(0, 0, 'backgroundintro');
    this.bgmSound = game.add.audio('start_bgm');
    this.bgmSound.play();
    this.bgmSound.loop = true; 


    //game.add.button(game.world.centerX - 95, 400, 'startbutton', this.start, this, 2, 1, 0);
    //var player = game.add.sprite(0, 300, 'playerInMenu');
    //game.add.tween(player).to({y: 295}, 500).to({y:
    //    305}, 1000).to({y: 300}, 500).loop().start();
    // Display the name of the game
    
    //var nameLabel = game.add.text(game.width/2, 80, 'Super Coin Box',{ font: '50px Arial', fill: '#ffffff' });
    //nameLabel.anchor.setTo(0.5, 0.5);
    // Show the score at the center of the screen
    //var scoreLabel = game.add.text(game.width/2, game.height/2,'score: ' + game.global.score, { font: '25px Arial', fill: '#ffffff' });
    //scoreLabel.anchor.setTo(0.5, 0.5);
    // Explain how to start the game
    //var startLabel = game.add.text(game.width/2, game.height-80,'press the up arrow key to start', { font: '25px Arial', fill: '#ffffff' });
    //startLabel.anchor.setTo(0.5, 0.5);
    // Create a new Phaser keyboard variable: the up arrow key
    // When pressed, call the 'start'
    var upKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
    upKey.onDown.add(this.start, this);
    },

    start: function() {
    // Start the actual game
    //localStorage.setItem('maxLevel', 1);
    //localStorage.setItem('recent_score', 0);
    
    game.time.events.add(
        500, // Start callback after 1000ms.
        function() {
            this.bgmSound.pause();
            game.state.start('menu');
        }, // Callback
        this);
    
    },

}; 