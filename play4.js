var play4State = {
    preload: function() {
/*
        // Loat game sprites.
        game.load.image('background', 'assets/background.png');
        game.load.image('background2', 'raccoon/raccoon/introduction_background.png');
        game.load.image('ground', 'assets/ground.png');
        game.load.image('pixel', 'assets/pixel.png');
        game.load.image('coin','raccoon/small_coin.png');
        




        /// Load block spritesheet.
        game.load.spritesheet('block1', 'assets/block1.png', 28, 28);
        game.load.spritesheet('block2', 'assets/block2.png', 28, 28);
        game.load.spritesheet('bullet1', 'raccoon/beam.png', 19, 20);
        game.load.spritesheet('bullet_enemy', 'raccoon/bomb.png', 32, 32);
        game.load.spritesheet('enemy_bee1','raccoon/bee_animation.png',69,53);

        /// ToDo 1: Load spritesheet
        ///      The name of sprite is 'player'.
        ///      The spritesheet filename is assets/MARIO.png
        ///      A frame size is 32 x 54.
        game.load.spritesheet('player', 'assets/MARIO.png', 32, 54);
*/
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        //game.add.image(0, 0, 'background'); 
        this.tileSprite = game.add.tileSprite(0, 0, 800, 600, 'background2');
        var blurX = game.add.filter('BlurX');
        var blurY = game.add.filter('BlurY');
    
        blurX.blur = 2;
        blurY.blur = 2;
    
        this.tileSprite.filters = [blurX, blurY];
        //cursors = game.input.keyboard.createCursorKeys();

        /// ToDo 2: How can we enable physics in Phaser? (Mode : ARCADE)
        game.physics.startSystem(Phaser.Physics.ARCADE);
        ///

        game.renderer.renderSession.roundPixels = true;

        this.cursor = game.input.keyboard.createCursorKeys();
        this.spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        
        this.spaceKey.onDown.add(this.ultimate, this);
        //this.cursor.space = Phaser.Input.Keyboard.KeyCodes.SPACE;
        

        // Display the score
        this.scoreLabel = game.add.text(570, 40, 'score: 0',{ font: '18px Arial', fill: '#ffffff' });
        // Initialize the score variable
        this.score = 0;

        this.enemyLabel = game.add.text(570, 20, 'Enemy Killed: 0',{ font: '18px Arial', fill: '#ffffff' });
        this.beeKilled = 0;


        this.player = game.add.sprite(game.width/2, game.height/2, 'player');
        this.player.facingLeft = false;
        game.physics.arcade.enable(this.player);
        this.player.body.collideWorldBounds = true;
        this.player.blood = 100;
        this.player.animations.add('player_fly', [0, 1], 2, true);

        this.roar = game.add.sprite(game.width, game.height, 'ultimate');
        game.physics.arcade.enable(this.roar);
        this.roar.enableBody = true;

        this.roarEnable = game.add.sprite(740, 60, 'ultimate_icon');

        this.shootSound = game.add.audio('shoot');
        this.roarSound = game.add.audio('roar');
        game.bgmSound = game.add.audio('play_bgm');
        game.bgmSound.play();
        game.bgmSound.loop = true; 

        this.pauseBtn = game.add.button(700, 10, 'pause', this.pause, this, 2, 1, 0);
        this.muteBtn = game.add.button(750, 10, 'volume', this.mute, this, 2, 1, 0);


        this.bullets = game.add.group();
        this.bullets.enableBody = true;
        this.bullets.createMultiple(20, 'bullet1');
        this.bullets.setAll('checkWorldBounds' , true);
        this.bullets.setAll('outOfBoundsKill',true);
        
        game.time.events.loop(game.shootSpeed, this.addBullet, this);


        //enemy    
        var life = 3;
        this.ultimateFlag = 1;

        this.enemy = game.add.sprite(game.width-2, game.height/2, 'enemy_monster');
        this.enemy.enableBody = true;
        game.physics.arcade.enable(this.enemy);
        this.enemy.checkWorldBounds = true;
        this.enemy.outOfBoundsKill = true;
        this.enemy.life = 150;
        this.enemy.lifeMax = 150;
        //game.time.events.loop(1000, this.moveEnemy, this);
        this.enemy.body.collideWorldBounds = true;
        //this.moveEnemy();


        this.bulletsOfEnemy = game.add.group();
        this.bulletsOfEnemy.enableBody = true;
        this.bulletsOfEnemy.createMultiple(20, 'bullet_enemy');
        this.bulletsOfEnemy.setAll('checkWorldBounds' , true);
        this.bulletsOfEnemy.setAll('outOfBoundsKill',true) ;
        this.bulletsOfEnemy.setAll('anchor.x', 0);
        this.bulletsOfEnemy.setAll('anchor.y', 0.5);
        game.time.events.loop(500, this.addEnemyBullet, this);




        //add health bar
        var barConfig = {x: 200, y: 100};
        this.myHealthBar = new HealthBar(this.game, barConfig);
        this.myHealthBar.setPercent(this.player.blood); 
        this.myHealthBar.setPosition(150, 20);

        //this.enemy.add(this.myHealthBar);
        //this.myHealthBar.setToGroup(this.enemy);
        //healthbar.crop﻿.width = (character.health / character.maxHealth) * healthbar.width
        //this.myHealthBar.setupConfiguration.config(bgcolor = '#FF0000';
        //this.myHealthBar.color = '#00FF00';
        
        //COIN
        this.coin = game.add.group();
        this.coin.enableBody = true;
        this.coin.createMultiple(20, 'coin');
        this.coin.setAll('checkWorldBounds' , true);
        this.coin.setAll('outOfBoundsKill',true) ;
        this.coin.setAll('anchor.x', 0);
        this.coin.setAll('anchor.y', 0.5);
        //game.time.events.loop(1000, this.addEnemyBullet, this);



        //this.blueBlock.body.immovable = false;

        /// Particle
        this.emitter = game.add.emitter(0, 0, 15);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.gravity = 500;




    },

    beeDie: function(bullet, bee){
        //var vy = bee.body.velocity.y;
        bee.kill();
        this.enemy.life -= game.damage;
        //console.log(bee.life);
        this.addCoin(1,this.enemy.y); 
        
        //bee.body.velocity.x = -100;
        //bee.body.velocity.y = vy;
        console.log(this.enemy.life);
        if(this.enemy.life <= 0){
            var x = this.enemy.x+this.enemy.width/2;
            var y = this.enemy.y+this.enemy.height/2;
            var emitter1 = game.add.emitter(x,y , 30);
            emitter1.makeParticles('pixelgreen');
            emitter1.setYSpeed(-150, 150);
            emitter1.setXSpeed(-150, 150);
            emitter1.setScale(2, 0, 2, 0, 800);
            emitter1.gravity = 500;
    
            emitter1.start(true, 800, null, 100);
            this.enemy.kill();


            //this.enemy.kill();

            //this.addCoin(this.enemy.lifeMax,beey);   
            //var explosion = game.add.sprite(beex, beey, 'explosion');
            //explosion.anchor.setTo(0.5);
            //explosion.animations.add('boom');
            //explosion.play('boom', 15, false, true);
            game.time.events.add(
                1000, // Start callback after 1000ms.
                function() {
                    localStorage.setItem('maxLevel', 4);
                    game.state.start('win');
                }, // Callback
                this);
            
        }
        console.log(this.enemy.life);
        
    },
    ultimate:function(){
        if(this.ultimateFlag){
            this.roarEnable.alpha = 0;
            this.roarSound.play();
            this.ultimateFlag=0;
            var x = this.player.x;
            var y = this.player.y;
            this.roar.reset(x,y);
            this.roar.body.velocity.x = 400;
            this.roar.checkWorldBounds = true;
            this.roar.outOfBoundsKill = true;
            game.time.events.add(
                5000, // Start callback after 1000ms.
                function() {
                    this.ultimateFlag = 1;
                    this.roarEnable.alpha = 1;
                }, // Callback
                this);

        }
    },
    update: function() {
        this.tileSprite.tilePosition.x -= 2;
        this.moveEnemy();
        /// ToDo 6: Add collision 
        /// 1. Add collision between player and walls
        this.player.animations.play('player_fly',20,true);
        game.physics.arcade.collide(this.player, this.wall);
       
        game.physics.arcade.collide(this.bullets, this.enemy , this.beeDie, null, this);
        game.physics.arcade.collide(this.roar, this.enemy , this.killedByUltimate, null, this);

        game.physics.arcade.overlap(this.player, this.bulletsOfEnemy,this.loseBlood, null, this);
        game.physics.arcade.collide(this.player, this.enemy,this.loseBloodByEnemy, null, this);
        game.physics.arcade.overlap(this.player, this.coin,this.getCoin, null, this);

        // Play the animation.
        this.player.animations.play('player_fly',20,true);
        
        if (!this.player.inWorld) { this.playerDie();}
        this.movePlayer();
        if (!this.enemy.inWorld) { this.playerDie();}
        
    }, 
    pause:function(){
        if(!game.paused){
            game.paused = true;
            this.pauseBtn.alpha = 0.5;
        }
        else{
            game.paused = false;
            this.pauseBtn.alpha = 1;
        }
    },
    mute:function(){
        if(!game.sound.mute){
            game.sound.mute = true;
            this.muteBtn.alpha = 0.5;
        }
        else{
            game.sound.mute = false;
            this.muteBtn.alpha = 1;
        }
    },

    killedByUltimate:function(roar,enemy){
        roar.kill();
        
        this.enemy.life -= 2*game.damage;
        this.addCoin(3,this.enemy.y); 
        //console.log(bee.life);
        //bullet.kill();
        //enemy.body.velocity.x = -100;
        if(this.enemy.life <= 0){
            var x = this.enemy.x+this.enemy.width/2;
            var y = this.enemy.y+this.enemy.height/2;
            var emitter1 = game.add.emitter(x,y , 30);
            emitter1.makeParticles('pixelgreen');
            emitter1.setYSpeed(-150, 150);
            emitter1.setXSpeed(-150, 150);
            emitter1.setScale(2, 0, 2, 0, 800);
            emitter1.gravity = 500;
    
            emitter1.start(true, 800, null, 100);
            this.enemy.kill();


            //this.enemy.kill();

            //this.addCoin(this.enemy.lifeMax,beey);   
            //var explosion = game.add.sprite(beex, beey, 'explosion');
            //explosion.anchor.setTo(0.5);
            //explosion.animations.add('boom');
            //explosion.play('boom', 15, false, true);
            game.time.events.add(
                1000, // Start callback after 1000ms.
                function() {
                    localStorage.setItem('maxLevel', 4);
                    game.state.start('win');
                }, // Callback
                this);
            
        }


    },

    playerDie: function() { game.state.start('main');},

    loseBloodByEnemy:function(player,enemy){
        if (!player.hasOverlapped && !enemy.hasOverlapped) {
            player.hasOverlapped = true;
            enemy.hasOverlapped = true;
            this.player.blood -= game.heart;
            this.myHealthBar.setPercent(this.player.blood);
            if(this.player.blood<=0){
                var x = this.player.x+this.player.width/2;
                var y = this.player.y+this.player.height/2;
                var emitter1 = game.add.emitter(x,y , 15);
                emitter1.makeParticles('pixelred');
                emitter1.setYSpeed(-150, 150);
                emitter1.setXSpeed(-150, 150);
                emitter1.setScale(2, 0, 2, 0, 800);
                emitter1.gravity = 500;
        
                emitter1.start(true, 800, null, 15);
                this.player.kill();
                //add delay
                game.time.events.add(
                    1000, // Start callback after 1000ms.
                    function() {
                        localStorage.setItem('recent_score', this.score);
                        //this.bgmSound.pause();
                        game.state.start('lose');
                    }, // Callback
                    this);
                //game.state.start('lose');
            }
        }
    },
    loseBlood:function(player,bulletsOfEnemy){
        bulletsOfEnemy.kill();
        this.player.blood -= game.heart;
        this.myHealthBar.setPercent(this.player.blood);
        if(this.player.blood<=0){
            var x = this.player.x+this.player.width/2;
            var y = this.player.y+this.player.height/2;
            var emitter1 = game.add.emitter(x,y , 15);
            emitter1.makeParticles('pixelred');
            emitter1.setYSpeed(-150, 150);
            emitter1.setXSpeed(-150, 150);
            emitter1.setScale(2, 0, 2, 0, 800);
            emitter1.gravity = 500;
    
            emitter1.start(true, 800, null, 15);
            this.player.kill();
            //add delay
            game.time.events.add(
                1000, // Start callback after 1000ms.
                function() {
                    localStorage.setItem('recent_score', this.score);
                    //this.bgmSound.pause();
                    game.state.start('lose');
                }, // Callback
                this);
            //game.state.start('lose');
        }

    },
    
    addBullet: function(){
        var bullet = this.bullets.getFirstDead(); 
        if (!bullet) { return;}
//  initialize bullet
        bullet.anchor.setTo(0.5, 1);
        this.shootSound.play();
        var playerx = this.player.x + this.player.width;
        var playery = this.player.y + this.player.height/2 + 9;
        bullet.reset(playerx, playery);
        bullet.body.velocity.x = 400;



    },
    addCoin:function(value,y){
        var coin = this.coin.getFirstDead(); 
        if (!coin) { return;}
//  initialize bullet
        coin.anchor.setTo(0.5, 1);
        coin.value = value;

        coin.reset(game.width, y);
        coin.body.velocity.x = -100;
    },
    addEnemyBullet: function(){
        var bullet = this.bulletsOfEnemy.getFirstDead(); 
        if (!bullet) { return;}
//  initialize bullet
        bullet.anchor.setTo(0.5, 1);
        //this.shootSound.play();
        var playerx = this.enemy.x;
        var playery = this.enemy.y + this.enemy.height/2 + 9;
        bullet.reset(playerx, playery);
        var enemyx = this.enemy.x;
        var enemyy = this.enemy.y;
         playerx = this.player.x;
         playery = this.player.y;
        var vy = playery-enemyy;
        var vx = playerx-enemyx;
        var vx2 = Math.pow(vx,2);
        var vy2 = Math.pow(vy,2);
        var dist = Math.sqrt(vx2+vy2);
        //console.log(dist);
        vx = vx/dist;
        vy = vy/dist;
        //this.enemy.body.velocity.x = +100;
        bullet.body.velocity.x = (300)*vx;
        //console.log(this.enemy.body.velocity.x);
        bullet.body.velocity.y = (300)*(vy);

        //bullet.body.velocity.x = -200;
        
      
    },
    getCoin:function(player,coin) {
        coin.kill();
        this.score += coin.value;
        this.scoreLabel.text = 'score: ' + this.score; 
        if(this.score >= 30){
            localStorage.setItem('recent_score', this.score);
            //this.bgmSound.pause();
            localStorage.setItem('maxLevel', 4);
            //game.state.start('win');
        }

    },

    /// ToDo 7: Finish the 4 animation part.
    moveEnemy:function(){
        //console.log(this.enemy.life);
        var enemyx = this.enemy.x;
        var enemyy = this.enemy.y;
        var playerx = this.player.x;
        var playery = this.player.y;
        var vy = playery-enemyy;
        var vx = playerx-enemyx;
        var vx2 = Math.pow(vx,2);
        var vy2 = Math.pow(vy,2);
        var dist = Math.sqrt(vx2+vy2);
        //console.log(dist);
        vx = vx/dist;
        vy = vy/dist;
        //this.enemy.body.velocity.x = +100;
        this.enemy.body.velocity.x = (100)*vx;
        //console.log(this.enemy.body.velocity.x);
        this.enemy.body.velocity.y = (100)*(vy);
    },
    movePlayer: function() {
        if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -400;
            this.player.facingLeft = true;
            if(this.player.x <=0){
                this.player.x = 0;
            }

        }
        else if (this.cursor.right.isDown) { 
            this.player.body.velocity.x = 400;
            this.player.facingLeft = false;
            if(this.player.x <=0){
                this.player.x = 0;
            }

        }    

        // If the up arrow key is pressed, And the player is on the ground.
        else if (this.cursor.up.isDown) { 
           
                this.player.body.velocity.y = -400;
                if(this.player.x <=0){
                    this.player.x = 0;
                }
    
        }  
        else if (this.cursor.down.isDown) { 
            
                this.player.body.velocity.y = 400;
                if(this.player.x <=0){
                    this.player.x = 0;
                }
    
        }  
       
        else {
            // Stop the player 
            this.player.body.velocity.x = 0;
            this.player.body.velocity.y = 0;
            if(this.player.x <=0){
                this.player.x = 0;
            }

            if(this.player.facingLeft) {
                // Change player frame to 3 (Facing left)
                this.player.frame = 3;
            }else {
                // Change player frame to 1 (Facing right)
                this.player.frame = 1;
            }

        }    
    }
};