var playState = {
    preload: function() {
/*
        // Loat game sprites.
        game.load.image('background', 'assets/background.png');
        game.load.image('background2', 'raccoon/raccoon/introduction_background.png');
        game.load.image('ground', 'assets/ground.png');
        game.load.image('pixel', 'assets/pixel.png');
        game.load.image('coin','raccoon/small_coin.png');
        




        /// Load block spritesheet.
        game.load.spritesheet('block1', 'assets/block1.png', 28, 28);
        game.load.spritesheet('block2', 'assets/block2.png', 28, 28);
        game.load.spritesheet('bullet1', 'raccoon/beam.png', 19, 20);
        game.load.spritesheet('bullet_enemy', 'raccoon/bomb.png', 32, 32);
        game.load.spritesheet('enemy_bee1','raccoon/bee_animation.png',69,53);

        /// ToDo 1: Load spritesheet
        ///      The name of sprite is 'player'.
        ///      The spritesheet filename is assets/MARIO.png
        ///      A frame size is 32 x 54.
        game.load.spritesheet('player', 'assets/MARIO.png', 32, 54);
*/
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        //game.add.image(0, 0, 'background'); 
        this.tileSprite = game.add.tileSprite(0, 0, 800, 600, 'background2');
        var blurX = game.add.filter('BlurX');
        var blurY = game.add.filter('BlurY');
    
        blurX.blur = 2;
        blurY.blur = 2;
    
        this.tileSprite.filters = [blurX, blurY];
        //cursors = game.input.keyboard.createCursorKeys();

        /// ToDo 2: How can we enable physics in Phaser? (Mode : ARCADE)
        game.physics.startSystem(Phaser.Physics.ARCADE);
        ///

        game.renderer.renderSession.roundPixels = true;

        this.cursor = game.input.keyboard.createCursorKeys();
        this.spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        
        this.spaceKey.onDown.add(this.ultimate, this);
        //this.cursor.space = Phaser.Input.Keyboard.KeyCodes.SPACE;
        

        // Display the score
        this.scoreLabel = game.add.text(570, 20, 'score: 0',{ font: '18px Arial', fill: '#ffffff' });
        // Initialize the score variable
        this.score = 0;

        this.enemyLabel = game.add.text(570, 40, 'Enemy Killed: 0',{ font: '18px Arial', fill: '#ffffff' });
        this.beeKilled = 0;


        this.player = game.add.sprite(game.width/2, game.height/2, 'player');
        this.player.facingLeft = false;
        game.physics.arcade.enable(this.player);
        this.player.body.collideWorldBounds = true;
        this.player.blood = 100;
        this.player.hasOverlapped = false;
        this.player.animations.add('player_fly', [0, 1], 2, true);

        this.roar = game.add.sprite(game.width, game.height, 'ultimate');
        game.physics.arcade.enable(this.roar);
        this.roar.enableBody = true;

        this.roarEnable = game.add.sprite(740, 60, 'ultimate_icon');

        this.shootSound = game.add.audio('shoot');
        this.roarSound = game.add.audio('roar');
        game.bgmSound = game.add.audio('play_bgm');
        game.bgmSound.play();
        game.bgmSound.loop = true; 

        this.pauseBtn = game.add.button(700, 10, 'pause', this.pause, this, 2, 1, 0);
        this.muteBtn = game.add.button(750, 10, 'volume', this.mute, this, 2, 1, 0);

        /// ToDo 3: Add 4 animations.
        /// 1. Create the 'rightwalk' animation with frame rate = 8 by looping the frames 1 and 2
        //this.player.animations.add('rightwalk', [1, 2], 8, true);
        /// 2. Create the 'leftwalk' animation with frame rate = 8 by looping the frames 3 and 4
        //this.player.animations.add('leftwalk', [3, 4], 8, true);
        /// 3. Create the 'rightjump' animation with frame rate = 16 (frames 5 and 6 and no loop)
        //this.player.animations.add('rightjump', [5, 6], 8, false);
        /// 4. Create the 'leftjump' animation with frame rate = 16 (frames 7 and 8 and no loop)
        //this.player.animations.add('leftjump', [7, 8], 8, false);
        ///

/*
        /// Add a little yellow block :)
        this.yellowBlock = game.add.sprite(200, 320, 'block1');
        this.yellowBlock.animations.add('Yblockanim', [0, 1, 2, 3], 8,  true);
        game.physics.arcade.enable(this.yellowBlock);
        this.yellowBlock.body.immovable = true;  
        //this.yellowBlock.body      
        
        /// Add a little dark blue block ;)
        this.blueBlock = game.add.sprite(422, 320, 'block2');
        this.blueBlock.animations.add('Bblockanim', [0, 1, 2, 3], 8,  true);
        game.physics.arcade.enable(this.blueBlock);
        this.blueBlock.body.immovable = true;
*/
        //bullet
        //this.bullet = game.add.sprite(game.width/2,game.height/2, 'bullet1');
        //game.physics.arcade.enable(this.bullet);
        //this.bullet.reset(game.width/2, 0);
        this.bullets = game.add.group();
        this.bullets.enableBody = true;
        this.bullets.createMultiple(20, 'bullet1');
        this.bullets.setAll('checkWorldBounds' , true);
        this.bullets.setAll('outOfBoundsKill',true);
        game.time.events.loop(game.shootSpeed, this.addBullet, this);


        //enemy    
        var life = 3;
        this.ultimateFlag = 1;
        this.enemy = game.add.group();
        
        this.enemy.enableBody = true;
        this.enemy.createMultiple(20, 'enemy_bee1');
        //this.enemy.animations.add('Yblockanim', [0, 1, 2, 3], 8,  true);
        this.enemy.callAll('animations.add', 'animations', 'fly', [0, 1], 2, true);
        this.enemy.setAll('life' ,2);
        //this.enemy.setAll('body.collideWorldBounds' , true);
        game.time.events.loop(1500, this.addEnemy, this);
        //this.enemy.add(this.myHealthBar);
        /*this.enemy.setAll(
            //lifebar = this.myHealthBar
            'life',3
        );*/


        this.bulletsOfEnemy = game.add.group();
        this.bulletsOfEnemy.enableBody = true;
        this.bulletsOfEnemy.createMultiple(20, 'bullet_enemy');
        this.bulletsOfEnemy.setAll('checkWorldBounds' , true);
        this.bulletsOfEnemy.setAll('outOfBoundsKill',true) ;
        this.bulletsOfEnemy.setAll('anchor.x', 0);
        this.bulletsOfEnemy.setAll('anchor.y', 0.5);
        //game.time.events.loop(1000, this.addEnemyBullet, this);




        //add health bar
        var barConfig = {x: 200, y: 100};
        this.myHealthBar = new HealthBar(this.game, barConfig);
        this.myHealthBar.setPercent(this.player.blood); 
        this.myHealthBar.setPosition(150, 20);

        //this.enemy.add(this.myHealthBar);
        //this.myHealthBar.setToGroup(this.enemy);
        //healthbar.crop﻿.width = (character.health / character.maxHealth) * healthbar.width
        //this.myHealthBar.setupConfiguration.config(bgcolor = '#FF0000';
        //this.myHealthBar.color = '#00FF00';
        
        //COIN
        this.coin = game.add.group();
        this.coin.enableBody = true;
        this.coin.createMultiple(20, 'coin');
        this.coin.setAll('checkWorldBounds' , true);
        this.coin.setAll('outOfBoundsKill',true) ;
        this.coin.setAll('anchor.x', 0);
        this.coin.setAll('anchor.y', 0.5);
        //game.time.events.loop(1000, this.addEnemyBullet, this);



        //this.blueBlock.body.immovable = false;

        /// Particle
        this.emitter = game.add.emitter(0, 0, 15);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.gravity = 500;


        /// Add floor
        //this.floor = game.add.sprite(0, game.height - 30, 'ground'); 
        //game.physics.arcade.enable(this.floor);
        //this.floor.body.immovable = true;

        

        

        // Add vertical gravity to the player
        //this.player.body.gravity.y = 500;

    },
/*
    blockTween: function() {

        var yellowBlockOriginalX = this.yellowBlock.x;
        var yellowBlockOriginalY = this.yellowBlock.y;
        /// ToDo 4: Tween yellow block.
        ///     Add Tween here: game.add.tween(this.yellowBlock)....? 
        ///     Move block to 20px above its original place with duration 100 ms 
        ///     And move it back (yoyo function).
        game.add.tween(this.yellowBlock).to({x: yellowBlockOriginalX, y:yellowBlockOriginalY-20}, 100).yoyo(true).start();
        ///
    },

    blockParticle: function() {

        /// ToDo 5: Start our emitter.
        ///      1. We'll burst out all partice at once.
        ///      2. The particle's lifespan is 800 ms.
        ///      3. Set frequency to null since we will burst out all partice at once.
        ///      4. We'll launch 15 particle.
        var x = this.player.x+this.player.width/2;
        var y = this.player.y+this.player.height/2;
        this.emitter1 = game.add.emitter(x,y , 15);
        this.emitter1.makeParticles('pixel');
        this.emitter1.setYSpeed(-150, 150);
        this.emitter1.setXSpeed(-150, 150);
        this.emitter1.setScale(2, 0, 2, 0, 800);
        this.emitter1.gravity = 500;

        this.emitter1.start(true, 800, null, 15);
        ///
    },
*/
    beeDie: function(bullet, bee){
        var vy = bee.body.velocity.y;
        bee.life -= game.damage;
        //console.log(bee.life);
        bullet.kill();
        bee.body.velocity.x = -100;
        bee.body.velocity.y = vy;
        if(bee.life <= 0){
            var beex = bee.x;
            var beey = bee.y;
            bee.kill();
            this.beeKilled +=1 ;
            this.enemyLabel.text = 'Enemy Killed: ' + this.beeKilled; 
            if(this.beeKilled>=10){
                localStorage.setItem('recent_score', this.score);
                //this.bgmSound.pause();
                localStorage.setItem('maxLevel', 2);
                game.state.start('win');
            }
            this.addCoin(bee.lifeMax,beey);   
            var explosion = game.add.sprite(beex, beey, 'explosion');
            //explosion.anchor.setTo(0.5);
            explosion.animations.add('boom');
            explosion.play('boom', 15, false, true);
        }
        
    },
    ultimate:function(){
        if(this.ultimateFlag){
            this.roarEnable.alpha = 0;
            this.roarSound.play();
            this.ultimateFlag=0;
            var x = this.player.x;
            var y = this.player.y;
            this.roar.reset(x,y);
            this.roar.body.velocity.x = 400;
            this.roar.checkWorldBounds = true;
            this.roar.outOfBoundsKill = true;
            game.time.events.add(
                5000, // Start callback after 1000ms.
                function() {
                    this.ultimateFlag = 1;
                    this.roarEnable.alpha = 1;
                }, // Callback
                this);

        }
    },
    update: function() {
        this.tileSprite.tilePosition.x -= 2;
        /// ToDo 6: Add collision 
        /// 1. Add collision between player and walls
        this.player.animations.play('player_fly',20,true);
        game.physics.arcade.collide(this.player, this.wall);
        /// 2. Add collision between player and floor
        //game.physics.arcade.collide(this.player, this.floor);
        /// 3. Add collision between player and yellowBlock and add trigger animation "blockTween"
        //game.physics.arcade.collide(this.player, this.yellowBlock, this.blockTween, null, this);
        /// 4. Add collision between player and blueBlock and add trigger animation "blockParticle"
        //game.physics.arcade.collide(this.player, this.blueBlock, this.blockParticle, null, this);
        ///
        game.physics.arcade.collide(this.bullets, this.enemy , this.beeDie, null, this);
        game.physics.arcade.collide(this.roar, this.enemy , this.killedByUltimate, null, this);

        game.physics.arcade.overlap(this.player, this.bulletsOfEnemy,this.loseBlood, null, this);
        game.physics.arcade.overlap(this.player, this.enemy,this.loseBloodByEnemy, null, this);
        game.physics.arcade.overlap(this.player, this.coin,this.getCoin, null, this);

        // Play the animation.
        this.player.animations.play('player_fly',20,true);
        //this.yellowBlock.animations.play('Yblockanim');
        //this.blueBlock.animations.play('Bblockanim',20,true);

        this.enemy.callAll('animations.play', 'animations', 'fly',20,true);

        if (!this.player.inWorld) { this.playerDie();}
        this.movePlayer();
    }, 
    pause:function(){
        if(!game.paused){
            game.paused = true;
            this.pauseBtn.alpha = 0.5;
        }
        else{
            game.paused = false;
            this.pauseBtn.alpha = 1;
        }
    },
    mute:function(){
        if(!game.sound.mute){
            game.sound.mute = true;
            this.muteBtn.alpha = 0.5;
        }
        else{
            game.sound.mute = false;
            this.muteBtn.alpha = 1;
        }
    },

    killedByUltimate:function(roar,enemy){
        //enemy.kill();
        
        enemy.life -= game.damage*2;
        //console.log(bee.life);
        //bullet.kill();
        enemy.body.velocity.x = -100;
        if(enemy.life <= 0){
            var x = enemy.x;
            var y = enemy.y;
            enemy.kill();
            this.beeKilled +=1 ;
            this.enemyLabel.text = 'Enemy Killed: ' + this.beeKilled; 
            if(this.beeKilled>=10){
                localStorage.setItem('recent_score', this.score);
                //this.bgmSound.pause();
                localStorage.setItem('maxLevel', 2);
                game.state.start('win');
            }
            this.addCoin(enemy.lifeMax,y);   
            var explosion = game.add.sprite(x, y, 'explosion');
            //explosion.anchor.setTo(0.5);
            explosion.animations.add('boom');
            explosion.play('boom', 15, false, true);
        }
        roar.body.velocity.x = 400;


    },

    playerDie: function() { game.state.start('main');},

    loseBloodByEnemy:function(player,enemy){
        console.log('enemy:  '+enemy.hasOverlapped);
        console.log('player:  '+player.hasOverlapped);
        if (!player.hasOverlapped && !enemy.hasOverlapped) {
            player.hasOverlapped = true;
            enemy.hasOverlapped = true;
            this.player.blood -= game.heart;
            this.myHealthBar.setPercent(this.player.blood);
            if(this.player.blood<=0){
                var x = this.player.x+this.player.width/2;
                var y = this.player.y+this.player.height/2;
                var emitter1 = game.add.emitter(x,y , 15);
                emitter1.makeParticles('pixelred');
                emitter1.setYSpeed(-150, 150);
                emitter1.setXSpeed(-150, 150);
                emitter1.setScale(2, 0, 2, 0, 800);
                emitter1.gravity = 500;
        
                emitter1.start(true, 800, null, 15);
                this.player.kill();
                //add delay
                game.time.events.add(
                    1000, // Start callback after 1000ms.
                    function() {
                        localStorage.setItem('recent_score', this.score);
                        //this.bgmSound.pause();
                        game.state.start('lose');
                    }, // Callback
                    this);
                //game.state.start('lose');
            }
        }
        player.hasOverlapped = false;
        //enemy.hasOverlapped = true;
    },
    loseBlood:function(player,bulletsOfEnemy){
        bulletsOfEnemy.kill();
        this.player.blood -= game.heart;
        this.myHealthBar.setPercent(this.player.blood);
        if(this.player.blood<=0){
            var x = this.player.x+this.player.width/2;
            var y = this.player.y+this.player.height/2;
            var emitter1 = game.add.emitter(x,y , 15);
            emitter1.makeParticles('pixelred');
            emitter1.setYSpeed(-150, 150);
            emitter1.setXSpeed(-150, 150);
            emitter1.setScale(2, 0, 2, 0, 800);
            emitter1.gravity = 500;
    
            emitter1.start(true, 800, null, 15);
            this.player.kill();
            //add delay
            game.time.events.add(
                1000, // Start callback after 1000ms.
                function() {
                    localStorage.setItem('recent_score', this.score);
                    //this.bgmSound.pause();
                    game.state.start('lose');
                }, // Callback
                this);
            //game.state.start('lose');
        }

    },
    addEnemy: function(){
        //this.enemy.add(this.myHealthBar);
        var enemy = this.enemy.getFirstDead();
        if (!enemy) { return;}
        enemy.anchor.setTo(0, 0);
        enemy.lifeMax = 3;
        enemy.life = enemy.lifeMax;
        enemy.hasOverlapped = false;
        game.physics.arcade.checkCollision.left = false;
        enemy.body.collideWorldBounds = true;
        
        //game.physics.arcade.checkCollision.top    = false;﻿
        enemy.onWorldBounds = true;
        enemy.body.bounce.y = 1;
        var enemyy = game.rnd.integerInRange(0, game.height/2);
        
        
        enemy.reset(game.width-enemy.width-1, enemyy*2);
        enemy.body.velocity.x = -100;
        enemy.body.velocity.y = -50 * game.rnd.pick([-1, 1]);
        enemy.checkWorldBounds = true;
        enemy.outOfBoundsKill = true;
        //console.log(enemy.life);
    },
    addBullet: function(){
        var bullet = this.bullets.getFirstDead(); 
        if (!bullet) { return;}
//  initialize bullet
        bullet.anchor.setTo(0.5, 1);
        this.shootSound.play();
        var playerx = this.player.x + this.player.width;
        var playery = this.player.y + this.player.height/2 + 9;
        bullet.reset(playerx, playery);
        bullet.body.velocity.x = 400;



    },
    addCoin:function(value,y){
        var coin = this.coin.getFirstDead(); 
        if (!coin) { return;}
//  initialize bullet
        coin.anchor.setTo(0.5, 1);
        coin.value = value;

        coin.reset(game.width, y);
        coin.body.velocity.x = -100;
    },
    addEnemyBullet: function(){
        //var bullet = this.bulletsOfEnemy.getFirstDead(); 
        //if (!bullet) { return;}
//  initialize bullet
        //bullet.anchor.setTo(0.5, 1);
        //var playerx = this.player.x + this.player.width;
        //var playery = this.player.y + this.player.height/2;
        /*for(i in this.enemy){
            enemyx=this.enemy.getChildAt(i).x;
            enemyy=this.enemy.getChildAt(i).y;
        }*/
        //var recentEnemy = this.enemy.getRandom(0,this.enemy.length);
        //console.log(1);
        
        this.enemy.forEachAlive(function(item){
            
            //this.bulletsOfEnemy = game.add.group();
            //this.bulletsOfEnemy.enableBody = true;
            ///this.bulletsOfEnemy.createMultiple(20, 'bullet_enemy');
            //game.time.events.loop(100, this.addEnemyBullet, this);
            var bullet = this.bulletsOfEnemy.getFirstDead(); 

            if (!bullet) { return;}
            bullet.anchor.setTo(0.5, 1);
            enemyx = item.x;
            enemyy = item.y+item.height/2;
            //console.log(enemyx);
            bullet.reset(enemyx, enemyy);
            bullet.body.velocity.x = -200;


        },this );
        //enemyx=recentEnemy.x;
        //enemyy=recentEnemy.y+ recentEnemy.height/2;
        //bullet.reset(enemyx, enemyy);
        //bullet.body.velocity.x = -400;
        //bullet.checkWorldBounds = true;
        //bullet.outOfBoundsKill = true;
    },
    getCoin:function(player,coin) {
        coin.kill();
        this.score += coin.value;
        this.scoreLabel.text = 'score: ' + this.score; 
        if(this.score >= 30){
            localStorage.setItem('recent_score', this.score);
            //this.bgmSound.pause();
            localStorage.setItem('maxLevel', 2);
            game.state.start('win');
        }

    },

    /// ToDo 7: Finish the 4 animation part.
    movePlayer: function() {
        if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -400;
            this.player.facingLeft = true;
            if(this.player.x <=0){
                this.player.x = 0;
            }

            /// 1. Play the animation 'leftwalk'
            //this.player.animations.play('leftwalk'); 
            ///
            //this.bullet.x = this.player.x+ this.player.width;
            //this.bullet.y = this.player.y + this.player.height/2;
            //this.shoot();
        }
        else if (this.cursor.right.isDown) { 
            this.player.body.velocity.x = 400;
            this.player.facingLeft = false;
            if(this.player.x <=0){
                this.player.x = 0;
            }

            /// 2. Play the animation 'rightwalk' 
            //this.player.animations.play('rightwalk'); 
            ///
        }    

        // If the up arrow key is pressed, And the player is on the ground.
        else if (this.cursor.up.isDown) { 
            //if(this.player.body.touching.down){
                // Move the player upward (jump)
                this.player.body.velocity.y = -400;
                if(this.player.x <=0){
                    this.player.x = 0;
                }
    
                /*if(this.player.facingLeft) {
                    /// 3. Play the 'leftjump' animation
                    this.player.animations.play('leftjump'); 
                    ///
                }else {
                    /// 4. Play the 'rightjump' animation
                    this.player.animations.play('rightjump'); 
                    ///
                }*/
                
            //}
        }  
        else if (this.cursor.down.isDown) { 
            //if(this.player.body.touching.down){
                // Move the player upward (jump)
                this.player.body.velocity.y = 400;
                if(this.player.x <=0){
                    this.player.x = 0;
                }
    
                /*if(this.player.facingLeft) {
                    /// 3. Play the 'leftjump' animation
                    this.player.animations.play('leftjump'); 
                    ///
                }else {
                    /// 4. Play the 'rightjump' animation
                    this.player.animations.play('rightjump'); 
                    ///
                }*/
                
            //}
        }  
        //else if(this.cursor.shift.isDown){
          //  this.shoot();
        //}
        // If neither the right or left arrow key is pressed
        else {
            // Stop the player 
            this.player.body.velocity.x = 0;
            this.player.body.velocity.y = 0;
            if(this.player.x <=0){
                this.player.x = 0;
            }

            if(this.player.facingLeft) {
                // Change player frame to 3 (Facing left)
                this.player.frame = 3;
            }else {
                // Change player frame to 1 (Facing right)
                this.player.frame = 1;
            }

            // Stop the animation
            this.player.animations.stop();
        }    
    }
};