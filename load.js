var loadState = {
    preload: function() {

        



        // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.width/2, 150,
            'loading...', { font: '30px Arial', fill: '#ffffff' });
            loadingLabel.anchor.setTo(0.5, 0.5);
            // Display the progress bar
        var progressBar = game.add.sprite(game.width/2, 200, 'progressBar');
            progressBar.anchor.setTo(0.5, 0.5);
            game.load.setPreloadSprite(progressBar);
        
        //game.load.image('background2', 'raccoon/raccoon/introduction_background.png');
        game.load.image('background2', 'raccoon/scrollingbkg.png');
        game.load.image('backgroundintro', 'raccoon/introduction_background.png');
        game.load.image('backgroundmenu', 'raccoon/background2-3.png');
        game.load.image('ground', 'assets/ground.png');
        game.load.image('pixel', 'assets/pixel.png');
        game.load.image('pixelred', 'raccoon/redpixel.png');
        game.load.image('pixelgreen', 'raccoon/greenpixel.png');
        game.load.image('coin','raccoon/small_coin.png');
        game.load.image('pause','raccoon/pause_btn.png');
        game.load.image('volume','raccoon/volumeBtn.png');
        game.load.image('damage','raccoon/damage.png');
        game.load.image('shooting_speed','raccoon/shooting_speed.png');
        game.load.image('heart','raccoon/heart.png');

        game.load.audio('start_bgm', 'assets/audio/bgm.mp3');
        game.load.audio('play_bgm', 'assets/audio/bgm_play.wav');
        game.load.audio('menu_bgm', 'assets/audio/bgm_menu.mp3');
        game.load.audio('shoot', 'assets/audio/shooting_short.wav');
        game.load.audio('roar', 'assets/audio/dragonRoar_short.wav');

        /// Load block spritesheet.
        game.load.spritesheet('block1', 'assets/block1.png', 28, 28);
        game.load.spritesheet('block2', 'assets/block2.png', 28, 28);
        game.load.spritesheet('bullet1', 'raccoon/bluefire.png', 32, 21);
        game.load.spritesheet('bullet_enemy', 'raccoon/beam2.png', 30, 30);
        game.load.spritesheet('ultimate', 'raccoon/roar.png', 100, 64);
        game.load.spritesheet('ultimate_icon', 'raccoon/roar_small.png', 50, 32);
        game.load.spritesheet('enemy_bee1','raccoon/bee_animation.png',69,53);
        game.load.spritesheet('enemy_egg','raccoon/egg.png',58,53);
        game.load.spritesheet('enemy_bat','raccoon/bat_animation.png',123,122);
        game.load.spritesheet('enemy_monster','raccoon/monster.png',150,131);
        game.load.spritesheet('explosion', 'assets/explosion.png', 53, 53);
        game.load.spritesheet('explosionBig', 'assets/explode.png', 26, 26);
        game.load.spritesheet('startbutton', 'raccoon/raccoon/playbutton.png', 172, 77);
        game.load.spritesheet('levelbutton1', 'raccoon/level1.png', 70, 70);
        game.load.spritesheet('levelbutton2', 'raccoon/level2.png', 70, 70);
        game.load.spritesheet('levelbutton3', 'raccoon/level3.png', 70, 70);
        game.load.spritesheet('levelbutton4', 'raccoon/level4.png', 70, 70);

        /// ToDo 1: Load spritesheet
        ///      The name of sprite is 'player'.
        ///      The spritesheet filename is assets/MARIO.png
        ///      A frame size is 32 x 54.
        game.load.spritesheet('player', 'raccoon/raccoon_resized.png', 80, 76);
        game.load.spritesheet('playerInMenu', 'raccoon/raccoon_mid.png', 300, 284);
        //this.tileSprite = game.add.tileSprite(0, 0, 800, 600, 'background2');
        game.load.script('BlurX', 'https://cdn.rawgit.com/photonstorm/phaser-ce/master/filters/BlurX.js');
        game.load.script('BlurY', 'https://cdn.rawgit.com/photonstorm/phaser-ce/master/filters/BlurY.js');

        game.load.image('background', 'raccoon/raccoon/background2-2.png',800,600);

    },




    create: function() {
    // Go to the menu state
    game.time.events.add(
        1000, // Start callback after 1000ms.
        function() {
            
            game.state.start('start');
        }, // Callback
        this);
    //game.state.start('start');
    }
}; 