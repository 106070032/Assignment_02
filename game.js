
var game = new Phaser.Game(800, 600, Phaser.AUTO, 'canvas');
//game.state.add('main', mainState);
game.global = { score: 0 }; 
game.state.add('win', winState);
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('start', startState);
game.state.add('intro', introState);
game.state.add('lose', loseState);
game.state.add('play', playState);
game.state.add('play2', play2State);
game.state.add('play3', play3State);
game.state.add('play4', play4State);

//game.state.add('win', winState);

game.state.start('boot');



